taxonomy_term_id is a module for Drupal 7.x that provides a solid and easy
accessible way to retrieve taxonomy term ids as array by passing taxonomy term title. 

calling method :
/**
 * Returns the taxanomy ids curresponding to the given terms as array.
 *
 */
 $terms = get_taxonomy_id($terms);


